import Message from '../Message/Message ';

export default function MessageList({ posts, onDeletePost, onEditPost }) {
  // const postsLength = this.state.posts.length;
  return (
    <ul>
      {posts.map(post => (
        <Message
          key={post.id}
          avatar={post.avatar}
          text={post.text}
          user={post.user}
          createdAt={post.createdAt}
          userId={post.userId}
          onDelete={() => onDeletePost(post.id)}
          onEdit={onEditPost}
          id={post.id}
        />
      ))}
    </ul>
  );
}
