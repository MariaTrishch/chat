import OwnMessage from '../OwnMessage/OwnMessage ';

export default function Message({
  id,
  avatar,
  text,
  user,
  createdAt,
  userId,
  onDelete,
  onEdit,
}) {
  return userId === '533b5230-1b8f-11e8-9629-c7eca82aa7bd' ? (
    <OwnMessage
      id={id}
      avatar={avatar}
      text={text}
      createdAt={createdAt}
      user={user}
      onDelete={onDelete}
      onEdit={onEdit}
    />
  ) : (
    <li key={id} className="message">
      <div className="list-message-item">
        <span className="message-user-avatar">
          <img src={avatar} alt="avatar"></img>
          {user}
        </span>
        <span className="message-text">{text}</span>
      </div>
      <div>
        <div>{createdAt}</div>
        <div>like</div>
      </div>
    </li>
  );
}
