export default function Header() {
  return (
    <header className="header">
      <ul>
        <li className="header-title">My chat</li>
        <li className="header-users-count">participants</li>
        <li className="header-messages-count">messages</li>
      </ul>
      <div className="header-last-message-date">last message at</div>
    </header>
  );
}
