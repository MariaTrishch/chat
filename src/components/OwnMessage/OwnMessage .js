import React, { Component } from 'react';

class OwnMessage extends Component {
  state = {
    message: '',
    editActive: false,
  };

  handleChange = e => {
    this.setState({ message: e.currentTarget.value });
  };

  handleSubmit = e => {
    e.preventDefault();

    this.props.onEdit(this.props.id, this.state.message);

    this.setState({ message: '' });
    this.editMessage();
  };

  editMessage = () => {
    this.setState(({ editActive }) => ({ editActive: !editActive }));
  };

  render() {
    return (
      <li key={this.props.id} className="own-message">
        <div className="message-user-avatar">
          <img src={this.props.avatar} alt="avatar"></img>
          <span>{this.props.user}</span>
        </div>
        {this.state.editActive ? (
          <form onSubmit={this.handleSubmit}>
            <textarea
              className="message-text"
              value={this.state.message}
              onChange={this.handleChange}
            ></textarea>

            <div className="own-message-buttons">
              <button type="submit" className="message-edit">
                Submit
              </button>
            </div>
          </form>
        ) : (
          <span>{this.props.text}</span>
        )}
        <button
          // type="submit"
          className="message-edit"
          onClick={this.editMessage}
        >
          Edit
        </button>
        <button
          // type="submit"
          className="message-delete"
          onClick={this.props.onDelete}
        >
          Delete
        </button>
        <div className="message-time">{this.props.createdAt}</div>
        <div>like</div>
      </li>
    );
  }
}

export default OwnMessage;
