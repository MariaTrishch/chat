import { Component } from 'react';
import axios from 'axios';
import logo from '../../img/logo.png';
import Header from '../Header/Header ';
import MessageList from '../MessageList/MessageList ';
import MessageInput from '../MessageInput/MessageInput ';

class Chat extends Component {
  state = {
    posts: [],
  };

  componentDidMount() {
    axios.get('http://localhost:3000/posts').then(response => {
      this.setState({ posts: response.data });
    });
  }

  addPost = message => {
    const post = {
      text: message,
      userId: '533b5230-1b8f-11e8-9629-c7eca82aa7bd',
      avatar:
        'https://resizing.flixster.com/EVAkglctn7E9B0hVKJrueplabuQ=/220x196/v1.cjs0NjYwNjtqOzE4NDk1OzEyMDA7MjIwOzE5Ng',
      user: 'Wendy',
      createdAt: '',
      editedAt: '',
    };

    axios.post('http://localhost:3000/posts', post).then(({ data }) => {
      this.setState(({ posts }) => ({ posts: [...posts, data] }));
    });
  };

  deletePost = postId => {
    axios.delete(`http://localhost:3000/posts/${postId}`).then(({ posts }) => {
      this.setState(({ posts }) => ({
        posts: posts.filter(({ id }) => id !== postId),
      }));
    });
  };

  editPost = (postId, message) => {
    const post = this.state.posts.find(({ id }) => id === postId);

    axios
      .patch(`http://localhost:3000/posts/${postId}`, { text: message })
      .then(({ data }) => {
        this.setState(({ posts }) => ({
          posts: posts.map(post => (post.id === data.id ? data : post)),
        }));
      });
  };

  render() {
    return (
      <>
        <div className="App">
          <img src={logo} alt="logo" width="35" />
        </div>
        <Header />
        <MessageList
          posts={this.state.posts}
          onDeletePost={this.deletePost}
          onEditPost={this.editPost}
        />
        <MessageInput onSubmit={this.addPost} />
      </>
    );
  }
}

export default Chat;
