import React, { Component } from 'react';

class MessageInput extends Component {
  state = {
    message: '',
  };

  handleChange = e => {
    this.setState({ message: e.currentTarget.value });
  };

  handleSubmit = e => {
    e.preventDefault();

    this.props.onSubmit(this.state.message);

    this.setState({ message: '' });
  };

  render() {
    return (
      <div className="message-input">
        <form onSubmit={this.handleSubmit}>
          <textarea
            className="message-input-text"
            value={this.state.message}
            onChange={this.handleChange}
          ></textarea>

          <button type="submit" className="message-input-button">
            Send
          </button>
        </form>
      </div>
    );
  }
}

export default MessageInput;
